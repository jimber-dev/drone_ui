var v = new Vue({
    el: '#app',
    data: {
        isPressed: false
    }
})


window.addEventListener("keydown", function (e) {
    e.preventDefault()
    key = e.which
    switch (key) {
        case 87:
            console.log("forward");
            break;
        case 68:
            console.log("right");
            break;
        case 83:
            console.log("reverse");
            break;
        case 65:
            console.log("left");
            break;
        case 38:
            console.log("up");
            break;
        case 39:
            console.log("right-y");
            break;
        case 40:
            console.log("down");
            break;
        case 37:
            console.log("left-y");
            break;
        default:
            break;

    }

}, false);